package com.example.dogsapp.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dogsapp.R;
import com.example.dogsapp.model.DogBreed;
import com.example.dogsapp.util.Util;
import com.example.dogsapp.viewModel.DetailViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DetailFragment extends Fragment {
    @BindView(R.id.dogImage)
    ImageView dogImage;

    @BindView(R.id.dogName)
    TextView dogName;

    @BindView(R.id.dogLifespan)
    TextView dogLifespan;

    @BindView(R.id.dogTemperament)
    TextView dogTemperament;

    @BindView(R.id.dogPurpose)
    TextView dogPurpose;

    private int dogUuid;
    private DetailViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null){
            dogUuid = DetailFragmentArgs.fromBundle(getArguments()).getDogUuid();
        }
        viewModel = ViewModelProviders.of(this).get(DetailViewModel.class);
        viewModel.fetch(dogUuid);

        observalViewModel();
    }

    private void observalViewModel() {
        viewModel.dogLiveData.observe(this, new Observer<DogBreed>() {
            @Override
            public void onChanged(DogBreed dog) {
                if (dog != null && dog instanceof DogBreed && getContext() != null){
                    dogName.setText(dog.dogBreed);
                    dogLifespan.setText(dog.lifespan);
                    dogTemperament.setText(dog.temperament);
                    dogPurpose.setText(dog.bredFor);


                    if (dog.imageUrl != null){
                        Util.loadImage(dogImage, dog.imageUrl, new CircularProgressDrawable(getContext()));
                    }
                }
            }
        });
    }
}