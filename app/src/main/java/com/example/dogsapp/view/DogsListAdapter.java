package com.example.dogsapp.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dogsapp.R;
import com.example.dogsapp.model.DogBreed;
import com.example.dogsapp.util.Util;

import java.util.ArrayList;
import java.util.List;

public class DogsListAdapter extends RecyclerView.Adapter<DogsListAdapter.DogsViewHoler> {
    private ArrayList<DogBreed> dogsList;

    public DogsListAdapter(ArrayList<DogBreed> dogsList){
        this.dogsList = dogsList;
    }

    public void updateDogsList(List<DogBreed> newDogsList){
        this.dogsList.clear();
        this.dogsList.addAll(newDogsList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DogsViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dog,parent,false);
        return new DogsViewHoler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DogsViewHoler holder, int position) {
        ImageView image = holder.itemView.findViewById(R.id.imageView);
        TextView name = holder.itemView.findViewById(R.id.name);
        TextView lifespan = holder.itemView.findViewById(R.id.lifespan);
        LinearLayout layout = holder.itemView.findViewById(R.id.dogLayout);

        name.setText(dogsList.get(position).dogBreed);
        lifespan.setText(dogsList.get(position).lifespan);
        Util.loadImage(image,dogsList.get(position).imageUrl, Util.getProgressDrawable(image.getContext()));
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListFragmentDirections.ActionDetail action = ListFragmentDirections.actionDetail();
                action.setDogUuid(dogsList.get(position).uuid);
                Navigation.findNavController(layout).navigate(action);

            }
        });
    }

    @Override
    public int getItemCount() {
        return dogsList.size();
    }

    class DogsViewHoler extends RecyclerView.ViewHolder{
        public View itemView;
        public DogsViewHoler(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }

}
